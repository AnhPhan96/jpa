package com.example.springdemo.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class BookAuthorId implements Serializable{
    private Long bookId;
    private Long AuthorId;

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getAuthorId() {
        return AuthorId;
    }

    public void setAuthorId(Long authorId) {
        AuthorId = authorId;
    }
}
