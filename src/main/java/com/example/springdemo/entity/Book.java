package com.example.springdemo.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "book")
public class Book {

    private Long id;
    private String name;
//    private List<Author> authors;
    private List<BookAuthor> bookAuthors = new ArrayList<>();


//    @ManyToMany
//    @JoinTable(name = "book_author",
//            joinColumns = {
//                @JoinColumn(name = "fk_book")
//            },
//            inverseJoinColumns = {
//                @JoinColumn(name = "fk_author")
//            }
//    )
//    public List<Author> getAuthors() {
//        return authors;
//    }

//    public void setAuthors(List<Author> authors) {
//        this.authors = authors;
//    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    public Long getId() {
        return id;
    }

    @OneToMany(mappedBy = "book")
    public List<BookAuthor> getBookAuthors() {
        return bookAuthors;
    }

    public void setBookAuthors(List<BookAuthor> bookAuthors) {
        this.bookAuthors = bookAuthors;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
