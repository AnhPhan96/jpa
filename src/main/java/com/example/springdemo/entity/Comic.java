package com.example.springdemo.entity;

import javax.persistence.*;

@Entity
@Table(name = "comic")
public class Comic {
    private Long id;
    private String name;
    private Author author;

    @Id
    @Column(name = "Comic_Id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @ManyToOne
    @JoinColumn(name = "id")
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
