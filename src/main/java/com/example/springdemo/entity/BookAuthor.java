package com.example.springdemo.entity;

import javax.persistence.*;

@Entity
@Table(name = "bookauthor")
public class BookAuthor {
    @EmbeddedId
    private BookAuthorId id;

    @ManyToOne
    @JoinColumn(name = "fk_book")
    @MapsId("id")
    private Book book;
    @ManyToOne
    @JoinColumn(name = "fk_author")
    @MapsId("id")
    private Author author;

    public BookAuthorId getId() {
        return id;
    }

    public void setId(BookAuthorId id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }


    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
